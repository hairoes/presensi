<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('m_mahasiswa', 'mhs');
    if ($this->session->userdata('isLogin') == FALSE) {
      redirect('login');
    }
  }


  public function Scanner()
  {
    $id = $this->uri->segment(3);
    $data['header_content'] = 'Scanner';
    $this->template->write_view('content', 'view_scanner', $data, TRUE);
    $this->template->render();
  }
  
  public function index()
  {
    // $this->load->model('m_akademik', 'akademik');
    // $nim = $this->session->userdata('nim');
    // $tahun = $this->session->userdata('id_tahun_akademik');
    // $kelas = $this->akademik->get_kelas($nim, $tahun);
    // $data['jadwal'] = $this->akademik->get_jadwal($kelas->kelas, $kelas->kode_makul, $tahun);
    $this->load->model('m_akademik', 'akademik');
    $nim = $this->session->userdata('nim');
    $tahun = $this->session->userdata('tahun_akademik');
    $data['kelas'] = $this->akademik->get_kelas($nim, $tahun);
    $data['header_content'] = 'Jadwal';
    $this->template->write_view('content', 'view_list_kelas', $data, TRUE);
    $this->template->render();
  }

  public function ajax_load_jadwal()
  {
    $this->load->model('m_akademik', 'akademik');
    $nim = $this->session->userdata('nim');
    $tahun = $this->session->userdata('tahun_akademik');
    $kelas = $this->akademik->get_kelas($nim, $tahun);
    foreach ($kelas as $key) {
      $jadwal = $this->akademik->get_jadwal($key->kelas, $key->kode_makul, $tahun);
      echo "<tr>";
      echo "<td>".ucwords($jadwal->hari)."</td>";
      echo "<td>".$key->nama_makul."</td>";
      echo "<td>".$jadwal->nama_dosen."</td>";
      // echo "<td align='center'><a class='btn btn-xs btn-primary' href='scanner/".$jadwal->id_jadwal."'>scanner</a></td>";
      echo "</tr>";
    }
  }
  
  public function do_presensi()
  {
    $nim = $this->session->userdata('nim');
    $tahun = $this->session->userdata('tahun_akademik');
    $result = $this->mhs->check_pembayaran($nim, $tahun);
    if ($result->status == 1) {
      $data = $this->input->post('data');
      $keys = $this->mhs->get_keys();
      if ($keys->status == 1) {
        /*Load library and generate chipertext*/
        $this->load->library('rsa');
        $decoded = $this->rsa->rsa_decrypt(base64_decode($data),  $keys->private,  $keys->modulo); //
        if ($decoded) {
          $cek = $this->mhs->cek_presensi($nim, $decoded);
          if($cek){
            echo json_encode(array('result' => false));
          }else{
            $params = array(
              'nim' => $nim, 
              'jenis' => $keys->jenis_ujian, 
              'id_jadwal' => $decoded, 
              'jam' => date('H:i'));	
            $this->mhs->do_presensi($params);
            echo json_encode(array('result' => true));
          }
        }
      }
    }else{
      echo json_encode(array('result' => 'tidak_lunas'));
    }
  }
}
