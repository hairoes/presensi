<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mahasiswa extends CI_Model{

  protected $table = 'mahasiswa';

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get_prodi()
  { 
    $this->db->order_by('nama_prodi', 'asc');
    $query = $this->db->get('prodi');
    return $query->result();
  }

  public function get_mahasiswa()
  { 
    // $this->db->join('prodi', 'prodi.id_prodi=mahasiswa.id_prodi');
    $this->db->order_by('nim', 'asc');
    $query = $this->db->get('mahasiswa');
    return $query->result();
  }

  public function get_mahasiswa_by_nim($nim)
  { 
    // $this->db->join('prodi', 'prodi.id_prodi=mahasiswa.id_prodi');
    $this->db->where('nim', $nim);
    $query = $this->db->get('mahasiswa');
    return $query->row();
  }

  public function get_mahasiswa_by_prodi($id)
  { 
    $this->db->join('prodi', 'prodi.id_prodi=mahasiswa.id_prodi', 'RIGHT');
    $this->db->where('mahasiswa.id_prodi', $id);
    $this->db->order_by('nim', 'asc');
    $query = $this->db->get('mahasiswa');
    return $query->result();
  }

  public function add_mahasiswa()
  {
    $nim        = $this->input->post('nim');
    $nama       = $this->input->post('nama');
    $prodi      = $this->input->post('prodi');
    $angkatan   = $this->input->post('angkatan');
    $kelas      = $this->input->post('kelas');
    $data = array('nim' => $nim, 'nama_mahasiswa' => ucwords($nama), 'prodi' => $prodi, 'angkatan' => $angkatan, 'kelas' => $kelas, 'password' => sha1($nim));
    return $this->db->insert('mahasiswa', $data);
  }

  public function delete()
  {
    $id = $this->uri->segment(3);
    $this->db->where('nim', $id);
    $this->db->delete('mahasiswa');
  }

  public function ajax_load_mahasiswa($prodi, $angkatan)
  {
    $query = $this->db->get_where('mahasiswa', array('prodi' => $prodi, 'angkatan' => $angkatan));
    return $query->result();
  }

  public function cek_presensi($nim, $jadwal)
  {
    return $this->db->get_where('presensi', array('nim' => $nim , 'id_jadwal' => $jadwal) )->row();
  }
}