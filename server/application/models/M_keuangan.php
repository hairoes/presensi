<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_keuangan extends CI_Model{

  protected $table = 'pembayaran';
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function get_setting()
  {
    return $this->db->get('setting')->row();
  }

  public function add_data_keuangan()
  {
    $data = array(
      'nim' => $this->input->post('nim'),
      'id_tahun_akademik' => $this->input->post('tahun-akademik'),
      'status' => $this->input->post('status'));
    $this->db->insert($this->table, $data);
    return true;
  }
  
  public function get_data_keuangan($tahun)
  {
    $nim = $this->input->post('cari-nim');
    $this->db->join('mahasiswa', 'mahasiswa.nim=pembayaran.nim');
    $this->db->join('tahun_akademik', 'tahun_akademik.id_tahun_akademik='.$tahun.'');
    return $this->db->get_where($this->table, array('pembayaran.nim' => $nim, 'pembayaran.id_tahun_akademik' => $tahun))->row();
  }

  public function update_data_keuangan($nim)
  {
    $data = array('status' => 1);
    $this->db->where('nim' , $nim);
    $this->db->update($this->table, $data);
    return true;
  }
}