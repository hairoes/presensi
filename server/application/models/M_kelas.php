<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kelas extends CI_Model{

  protected $table = 'kelas';
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function delete($id)
  {
    $this->db->where('id_kelas', $id);
    $this->db->delete($this->table);
  }

  public function add_kelas($id)
  {

    $makul     = $this->input->post('makul');
    $dosen      = $this->input->post('dosen');
    $hari      = $this->input->post('hari');
    $kelas      = $this->input->post('kelas');
    $data = array(
      'kode_makul' => $makul, 
      'kode_dosen' => $dosen, 
      'hari' => $hari,
      'kelas' => $kelas,
      'id_tahun_akademik' => $id);
    return $this->db->insert('jadwal', $data);
  }

  public function get_kelas($id)
  {
    $this->db->join('makul', 'makul.kode_makul=kelas.kode_makul', 'left');
    $this->db->join('dosen', 'dosen.kode_dosen=kelas.kode_dosen', 'left');
    $this->db->join('prodi', 'prodi.id_prodi=kelas.id_prodi', 'left');
    $this->db->where('kelas.id_tahun_akademik', $id);
    return $this->db->get($this->table)->result();
  }

  

  public function get_tahun_akademik()
  {
    $this->db->order_by('tahun', 'desc');
    return $this->db->get('tahun_akademik')->result();
  }

  public function get_setting_app()
  {
    return $this->db->get('setting')->row();
  }
}