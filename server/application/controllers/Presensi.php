<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_presensi', 'presensi');
		$this->load->model('m_akademik', 'akademik');
	}

	public function index()
	{
		$data['makul'] = $this->akademik->get_makul();
		$data['tahun_akademik'] = $this->akademik->get_tahun_akademik();
        $this->template->content->view('akademik/view_presensi', $data);
        $this->template->publish();
	}

	public function print_uts()
	{
		$this->load->library('pdf');
		$id = $this->uri->segment(3);
		$data = $this->akademik->ajax_cek_kelas($id);
		if (isset($data)) {
			$periode = $data->periode==1?'GANJIL':'GENAP';

			$pdf = new FPDF('p','mm','a4');
			$pdf->AddPage();
			$pdf->setTitle('Presensi Ujian Tengah Semester');
			// $pdf->Image('assets/images/logo.png',20,10,-900);
			$pdf->SetFont('Arial','B',16);
			$jenis = $this->session->userdata('jenis_ujian') == 1 ? 'UJIAN TENGAH SEMESTER' : 'UJIAN AKHIR SEMESTER';
			$pdf->Cell(200, 5, 'PRESENSI '.$jenis,0,1,'C');
			$pdf->SetFont('Arial','',14);
			$pdf->Cell(200, 5, 'FAKULTAS TEKNIK UNIVERSITAS NURUL JADID',0,1,'C');
			// $ket = $tmp->periode == 1 ? 'GANJIL' : 'GENAP';
			$pdf->Cell(200, 5, 'TAHUN AKADEMIK '.$periode,0,1,'C');

			$pdf->Line(10,30,200,30);
			$pdf->Line(10,31,200,31);
			$pdf->Cell(37, 10,'',0,1);
			$pdf->SetFont('Arial','',12);
			$pdf->Cell(40,5,'Kode',0,0);
			$pdf->Cell(50,5,strtoupper(': '.$data->kode_makul),0,1);
			$pdf->Cell(40,5,'Mata Kuliah',0,0);
			$pdf->Cell(50,5,ucwords(': '.$data->nama_makul),0,1);
			$pdf->Cell(40,5,'Kelas',0,0);
			$pdf->Cell(50,5,strtoupper(': '.$data->kelas),0,1);
			$pdf->Cell(40,5,'',0,1);
			$this->load->model('m_dosen', 'dosen');
			$krs = $this->dosen->ajax_get_krs_mhs($data->kode_makul, $data->kelas, $this->session->userdata('id_tahun_akademik'));
			
			$pdf->SetFont('Arial','B',12);
			$pdf->Cell(10,10,'NO',1,0, 'C');
			$pdf->Cell(35,10,'NIM',1,0, 'C');
			$pdf->Cell(70,10,'NAMA MAHASISWA',1,0, 'C');
			$pdf->Cell(30,10,'STATUS',1,0, 'C');
			$pdf->Cell(40,10,'KETERANGAN',1,1, 'C');
			$pdf->SetFont('Arial','',12);
			if (isset($krs)) {
				$no =1;
				foreach ($krs as $key) {
					$cek = $this->dosen->ajax_cek_presensi($key->nim, $id, 1);
					$status = $cek->num_rows() > 0 ?'Hadir':'-';
					$pdf->Cell(10,8,$no,1,0,'C');
					$pdf->Cell(35,8,$key->nim,1,0,'C');
					$pdf->Cell(70,8,$key->nama_mahasiswa,1,0);
					$pdf->Cell(30,8,$status,1,0,'C');
					$pdf->Cell(40,8,'',1,1, 'C');
					$no++;
				}
				
			}
	        $pdf->Output('',''.base64_encode($id).'-UTS.pdf');
		}     
	}

	public function print_uas()
	{
		$this->load->library('pdf');
		$id = $this->uri->segment(3);
		$data = $this->akademik->ajax_cek_kelas($id);
		if (isset($data)) {
			$periode = $data->periode==1?'GANJIL':'GENAP';

			$pdf = new FPDF('p','mm','a4');
			$pdf->AddPage();
			$pdf->setTitle('Presensi Ujian Tengah Semester');
			// $pdf->Image('assets/images/logo.png',20,10,-900);
			$pdf->SetFont('Arial','B',16);
			$jenis = $this->session->userdata('jenis_ujian') == 1 ? 'UJIAN TENGAH SEMESTER' : 'UJIAN AKHIR SEMESTER';
			$pdf->Cell(200, 5, 'PRESENSI '.$jenis,0,1,'C');
			$pdf->SetFont('Arial','',14);
			$pdf->Cell(200, 5, 'FAKULTAS TEKNIK UNIVERSITAS NURUL JADID',0,1,'C');
			// $ket = $tmp->periode == 1 ? 'GANJIL' : 'GENAP';
			$pdf->Cell(200, 5, 'TAHUN AKADEMIK '.$periode,0,1,'C');

			$pdf->Line(10,30,200,30);
			$pdf->Line(10,31,200,31);
			$pdf->Cell(37, 10,'',0,1);
			$pdf->SetFont('Arial','',12);
			$pdf->Cell(40,5,'Kode',0,0);
			$pdf->Cell(50,5,strtoupper(': '.$data->kode_makul),0,1);
			$pdf->Cell(40,5,'Mata Kuliah',0,0);
			$pdf->Cell(50,5,ucwords(': '.$data->nama_makul),0,1);
			$pdf->Cell(40,5,'Kelas',0,0);
			$pdf->Cell(50,5,strtoupper(': '.$data->kelas),0,1);
			$pdf->Cell(40,5,'',0,1);
			$this->load->model('m_dosen', 'dosen');
			$krs = $this->dosen->ajax_get_krs_mhs($data->kode_makul, $data->kelas, $this->session->userdata('id_tahun_akademik'));
			
			$pdf->SetFont('Arial','B',12);
			$pdf->Cell(10,10,'NO',1,0, 'C');
			$pdf->Cell(35,10,'NIM',1,0, 'C');
			$pdf->Cell(70,10,'NAMA MAHASISWA',1,0, 'C');
			$pdf->Cell(30,10,'STATUS',1,0, 'C');
			$pdf->Cell(40,10,'KETERANGAN',1,1, 'C');
			$pdf->SetFont('Arial','',12);
			if (isset($krs)) {
				$no =1;
				foreach ($krs as $key) {
					$cek = $this->dosen->ajax_cek_presensi($key->nim, $id, 2);
					$status = $cek->num_rows() > 0 ?'Hadir':'-';
					$pdf->Cell(10,8,$no,1,0,'C');
					$pdf->Cell(35,8,$key->nim,1,0,'C');
					$pdf->Cell(70,8,$key->nama_mahasiswa,1,0);
					$pdf->Cell(30,8,$status,1,0,'C');
					$pdf->Cell(40,8,'',1,1, 'C');
					$no++;
				}
				
			}
	        $pdf->Output('',''.base64_encode($id).'-UAS.pdf');
		}     
	}
}
