<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_dosen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_dosen', 'dosen');
	}

	public function ajax_load_presensi()
	{
		$id = $this->input->post('id');
		$makul = $this->input->post('makul');
		$kelas = $this->input->post('kelas');
		$krs = $this->dosen->ajax_get_krs_mhs($makul, $kelas, $this->session->userdata('id_tahun_akademik'));
		$setting = $this->dosen->get_setting();
		if (isset($krs)) {
			$no =1;
			foreach ($krs as $key) {
				$cek = $this->dosen->ajax_cek_presensi($key->nim, $id, $setting->jenis_ujian);
				$status = $cek->num_rows() > 0 ?'<a class="text-success">Hadir</a>':'<a class="text-danger">Tidak</a>';
				echo "<tr>";
				echo "<td>".$no."</td>";
				echo "<td>".$key->nim."</td>";
				echo "<td>".$key->nama_mahasiswa."</td>";
				echo "<td>".$status."</td>";
				echo "</tr>";
				$no++;
			}
			
		}
	}

	public function ajax_load_jadwal()
	{
		$tahun = $this->session->userdata('id_tahun_akademik');
		$dosen = $this->session->userdata('kode_dosen');
		$hari = array('senin','selasa','rabu','kamis','jumat','sabtu','minggu');
		for ($i=0; $i < count($hari) ; $i++) { 
			$jadwal = $this->dosen->ajax_load_jadwal($tahun, $hari[$i], $dosen);
			if ($jadwal->num_rows() > 0) {
					
				echo "<tr>";
				$row = $jadwal->num_rows() + 1;
				echo "<td rowspan=".$row.">".ucwords($hari[$i])."</td>";
				foreach ($jadwal->result() as $key) {
					echo "<tr>";
					echo "<td >".$key->nama_makul."</td>";
					echo "<td align=center>".ucwords($key->kelas)."</td>";
					// echo "<td >".$key->nama_dosen."</td>";
					echo "<td align=center><a href='#'  onclick=load_presensi('".$key->id_jadwal."','".$key->kode_makul."','".$key->kelas."')><i class='fa fa-eye'/></a>
					<a  href='".base_url()."dosen/view_qr/".$key->id_jadwal."'><i class='fa fa-qrcode'/></a></td>";
					echo "</tr>";
				}
				echo "</tr>";
			}
		}
	}
}