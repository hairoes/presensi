<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_user', 'user');
	}

	public function index()
	{
		$data['user'] = $this->user->get_user();
		$this->template->content->view('akademik/view_user', $data);
        $this->template->publish();
	}

	public function delete()
	{	
		$id = $this->uri->segment(3);
		$this->user->delete($id);
		redirect('user');
	}

	public function add()
	{
		if ($this->input->post()) {
			$this->user->add_user();
		}
		redirect('user');
	}

	
}
