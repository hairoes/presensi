<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_keuangan', 'keuangan');
	}


	public function index()
	{
		$this->load->model('m_akademik', 'akademik');
		$data['tahun_akademik'] = $this->akademik->get_tahun_akademik();
		$this->template->content->view('akademik/add_keuangan', $data);
		if ($this->input->post()) {
			$setting = $this->keuangan->get_setting();
			$result = $this->keuangan->get_data_keuangan($setting->id_tahun_akademik);
			if ($result) {
				$data['keuangan'] = $result;
				$this->template->content->view('akademik/view_data_keuangan', $data);
			}else{
				echo "<script>alert('Data tidak ditemukan!');</script>";
			}
			
		}
		$this->template->publish();
	}

	public function add()
	{
		if ($this->input->post()) {
			$this->keuangan->add_data_keuangan();
		}
		redirect('keuangan');
	}

	public function update()
	{
		$nim = $this->uri->segment(3);
		$result = $this->keuangan->update_data_keuangan($nim);
		if ($result) {
			redirect('Keuangan');
		}

	}
}
