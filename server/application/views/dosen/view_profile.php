<div class="col-md-6 col-sm-6 col-xs-12 profile_details col-md-offset-3">

<div class="well profile_view">
  <div class="col-sm-12">
    <h4 class="brief"><i></i></h4>
    <div class="left col-xs-7">
      <h2><?php echo strtoupper($dosen->nama_dosen) ?></h2>
      <p><strong>About: </strong> Dosen Teladan </p>
      <ul class="list-unstyled">
        <li><i class="fa fa-building"></i> Address: Paiton Probolinggo</li>
        <li><i class="fa fa-phone"></i> Phone #: - </li>
      </ul>
    </div>
    <div class="right col-xs-5 text-center">
      <img src="<?php echo base_url() ?>assets/images/img.jpg" alt="" class="img-circle img-responsive">
    </div>
  </div>
  <div class="col-xs-12 bottom text-center">
    <div class="col-xs-12 col-sm-12 emphasis">
      <form action="<?php echo base_url() ?>dosen/update_password" method="post">
        <div class="input-group">
          <input type="password" class="form-control" name="password" placeholder="Password">
            <span class="input-group-btn">
             <button type="submit"  class="btn btn-default">Update Password</button>
            </span>
        </div>
      </form>
    </div>
   
  </div>
</div>
 <?php if ( $this->session->flashdata('result') == true ):?>
    <div class="alert alert-success alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <strong>Success !</strong> Password was updated!.
    </div>
 <?php endif ?>
</div>

