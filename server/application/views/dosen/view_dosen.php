
<div class="">
  <a href="<?php echo base_url() ?>dosen/add" class="btn btn-sm btn-default" >Tambah</a>
  <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="row x_title">
            <div class="col-md-6">
              <h3>List Dosen</h3>
            </div>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          <div class="table-responsive">
            <table class="table table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nip</th>
                  <th>Nama Lengkap</th>
                  <th>Username</th>
                  <th width="50" align="center">Option</th>

                </tr>
              </thead>
              <tbody id="show-data">
                <?php $no=1 ?>
                <?php foreach ($dosen as $key): ?>
                  <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $key->nip ?></td>
                    <td><?php echo $key->nama_dosen ?></td>
                    <td><?php echo $key->username ?></td>
                    <td align="center">
                    <div class="button-group">
                      <a href="<?php echo base_url()."dosen/delete/".$key->kode_dosen.""?>"><i class="fa fa-trash"></i></a>
                    </div>
                  </td>
                  </tr>
                  <?php $no++ ?>
                <?php endforeach ?>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>