<!doctype html>
<html>
<head>
    <title><?php echo $this->template->title->default("Aplikasi Presensi Ujian"); ?></title>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="description" content="<?php echo $this->template->description; ?>">
    <meta name="author" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('assets/css/nprogress.css')?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url('assets/css/blue.css')?>" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url('assets/css/bootstrap-progressbar-3.3.4.min.css')?>" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url('assets/css/jqvmap.min.css')?>" rel="stylesheet"/>
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('assets/css/custom.css')?>" rel="stylesheet">
    

    <?php echo $this->template->meta; ?>
    <?php echo $this->template->stylesheet; ?>
    <!-- jQuery -->
    <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
     <?php echo $this->template->javascript; ?>
</head>
<body class="nav-md footer_fixed">
  <div class="container body">
    <div class="main_container">
      <!-- left column -->
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title  text-center" style="border: 0;">
            <a href="#" class="site_title"><span>Aplikasi Presensi</span></a>
          </div>

          <div class="clearfix"></div>

          <br />
          <!-- sidebar menu -->
          <?php echo $this->template->widget("sidebar_menu");?>
         
          <!-- /menu footer buttons -->
        </div>
      </div>
      <!-- /left column -->
      <!-- top navigation -->
      <?php  echo $this->template->widget("navbar_top");?>
      <!-- /top navigation -->
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="clearfix"></div>
        <?php echo $this->template->content;?>
      </div>
      <!-- /page content -->
  </div>
 <!-- footer content -->
  <footer>
    <div class="pull-right">
      <?php
        $copy = '&copy '.date('Y');
        echo $this->template->footer->prepend($copy." Aplikasi Presensi V.beta");?>
    </div>
    <div class="clearfix"></div>
  </footer>
  <!-- /footer content -->

</div>

<!-- Bootstrap -->
<script src="<?php echo base_url('vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('vendors/fastclick/lib/fastclick.js')?>"></script>
<!-- NProgress -->
<script src="<?php echo base_url('vendors/nprogress/nprogress.js')?>"></script>
<!-- Chart.js -->
<script src="<?php echo base_url('vendors/Chart.js/dist/Chart.min.js')?>"></script>
<!-- gauge.js -->
<script src="<?php echo base_url('vendors/gauge.js/dist/gauge.min.js')?>"></script>
<!-- bootstrap-progressbar -->
<script src="<?php echo base_url('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('vendors/iCheck/icheck.min.js')?>"></script>
<!-- Skycons -->
<script src="<?php echo base_url('vendors/skycons/skycons.js')?>"></script>
<!-- Flot -->
<script src="<?php echo base_url('vendors/Flot/jquery.flot.js')?>"></script>
<script src="<?php echo base_url('vendors/Flot/jquery.flot.pie.js')?>"></script>
<script src="<?php echo base_url('vendors/Flot/jquery.flot.time.js')?>"></script>
<script src="<?php echo base_url('vendors/Flot/jquery.flot.stack.js')?>"></script>
<script src="<?php echo base_url('vendors/Flot/jquery.flot.resize.js')?>"></script>
<!-- Flot plugins -->
<script src="<?php echo base_url('vendors/flot.orderbars/js/jquery.flot.orderBars.js')?>"></script>
<script src="<?php echo base_url('vendors/flot-spline/js/jquery.flot.spline.min.js')?>"></script>
<script src="<?php echo base_url('vendors/flot.curvedlines/curvedLines.js')?>"></script>
<!-- DateJS -->
<script src="<?php echo base_url('vendors/DateJS/build/date.js')?>"></script>
<!-- JQVMap -->
<script src="<?php echo base_url('vendors/jqvmap/dist/jquery.vmap.js')?>"></script>
<script src="<?php echo base_url('vendors/jqvmap/dist/maps/jquery.vmap.world.js')?>"></script>
<script src="<?php echo base_url('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')?>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url('assets/js/moment.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/daterangepicker.js')?>"></script>


<!-- Custom Theme Scripts -->
<script src="<?php echo base_url('assets/js/custom.js')?>"></script> 
<script>
  $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-success").slideUp(500);
});
</script>


   
</body>
</html>


