<!-- <div class="clearfix"></div> -->


<!-- <div class="row"> -->
  <div class="col-md-4">
    <form action="<?php echo base_url() ?>keuangan" method="post">
      <div class="input-group">
        <input class="form-control" type="text" name="cari-nim" placeholder="Cari Nim">
        <span class="input-group-btn">
          <button type="submit" class="btn btn-primary">Go!</button>
        </span>
      </div>
    </form>
    <div class="x_panel">
      <div class="x_title">
        <h2>Tambah Data Keuangan</small></h2>
        
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form method="post" action="<?php echo base_url('keuangan/add') ?>">

          <div class="form-group">
            <label  class="control-label">Nim<span class="required">*</span>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" required="required" class="form-control" name="nim">
            </div>
          </div>
          
          
          <div class="form-group">
            <label class="control-label">Tahun Akademik<span class="required">*</span>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select name="tahun-akademik" class="form-control" required="required">
                <?php foreach ($tahun_akademik as $key ): ?>
                  <option value="<?php echo $key->id_tahun_akademik ?>"><?php echo $key->tahun ?> - <?php echo $key->periode == 1 ? 'Ganjil' : 'Genap'?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label">Keterangan<span class="required">*</span>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <select name="status" id="" class="form-control" required="required">
              <option value="1">Lunas</option>
              <option value="0">Tidak Lunas</option>
              </select>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
              <button type="submit" class="btn btn-primary" name="submit">Tambah</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
<!-- </div> -->




            