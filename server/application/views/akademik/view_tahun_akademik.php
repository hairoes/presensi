
<div class="">
  
  <div class="clearfix"></div>
  
 
  
    <div class="row">
      <div class="col-md-4">
         
      
     <!-- <div class="col-md-6 col-sm-12 col-xs-12"> -->
        <div class="x_panel">
          <div class="row x_title">
          <div class="col-md-12">
            <h3>Daftar Tahun Akademik</h3>
          </div>
            
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table class="table table-responsive">
              <thead>
                <th>#</th>
                <th>Tahun</th>
                <th>Periode</th>
                <th width="50" align="center">Option</th>
              </thead>
              <tbody>
                <?php $no=1 ?>
                <?php foreach ($tahun_akademik as $key): ?>
                  <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $key->tahun ?></td>
                    <td><?php echo $key->periode==1?'Ganjil':'Genap' ?></td>
                    <td align="center">
                    <div class="button-group">
                      <a href="<?php echo base_url()."akademik/delete_tahun_akademik/".$key->id_tahun_akademik.""?>"><i class="fa fa-trash"></i></a>
                    </div>
                  </td>
                  </tr>
                  <?php $no++ ?>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      <!-- </div> -->
    </div>
  </div>
</div>
