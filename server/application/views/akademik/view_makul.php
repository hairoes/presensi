
<div class="">
  
  <div class="clearfix"></div>
    <a href="<?php echo base_url() ?>makul/add" class="btn btn-sm btn-default" >Tambah</a>
    <div class="row">
      
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="row x_title">
            <div class="col-md-6 col-xs-12">
              <h3>Data Mata Kuliah</h3>
            </div>
            
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          
          <table class="table   nowrap" id="table-makul">
            <thead>
              <tr>
                <th>Kode</th>
                <th>Nama Mata Kuliah</th>
                <th>SKS</th>
                <th>Semester</th>
                <th width="50" align="center">Option</th>
              </tr>
            </thead>
            <tbody id="show-data">
              <?php $no=1 ?>
              <?php foreach ($makul as $key): ?>
                <tr>
                  <td><?php echo strtoupper($key->kode_makul) ?></td>
                  <td><?php echo $key->nama_makul ?></td>
                  <td><?php echo $key->sks ?></td>
                  <td><?php echo $key->semester ?></td>
                  <td align="center">
                    <div class="button-group">
                      <a href="<?php echo base_url()."makul/delete/".$key->kode_makul.""?>"><i class="fa fa-trash"></i></a>
                    </div>
                  </td>
                </tr>
              <?php endforeach ?>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
