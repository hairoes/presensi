
<div class="">
  
  <div class="clearfix"></div>
    <a href="<?php echo base_url() ?>jadwal/add" class="btn btn-sm btn-default" >Tambah</a>
    <div class="row">
      
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="row x_title">
            <div class="col-md-3">
                 <div class="input-group">
                  <select name="tahun-akademik" id="" class="form-control" >
                    <option value="0">Tahun Akademik</option>
                  <?php foreach ($tahun_akademik as $key): ?>
                    <option value="<?php echo $key->id_tahun_akademik ?>"><?php echo $key->tahun ?> - <?php echo $key->periode==1?'Ganjil':'Genap' ?></option>
                  <?php endforeach ?>
                  </select>
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary" onclick="load_jadwal()">Tampil</button>
                  </span>
                </div> 
            </div>
            
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
         
            <table class="table table-bordered" >
            <thead>
                <th width="50">Hari</th>
                <th>Mata Kuliah</th>
                <th>Kelas</th>
                <th>Dosen</th>
                <th width="20" align="center">Option</th>
            </thead>
            <tbody id="show-data">
             
            </tbody>
          </table>
          
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function load_jadwal(){
    var th = $('[name=tahun-akademik]').val();
    if (th != 0) {
      $.ajax({
        url: "<?php echo base_url()?>ajax/ajax_akademik/ajax_load_jadwal",
        type: "post",
        data:{'tahun':th},
        success: function (html) {
          console.log(html);
          $('#show-data').html(html);
        }
      });
    }else{
      alert('Pilih Tahun Akademik');
    }
  }

  function delete_jadwal(id) {
    $.ajax({
      url: "<?php echo base_url()?>ajax/ajax_akademik/ajax_delete_jadwal",
      type: "post",
      data:{'id':id},
      success: function(data) {
        load_jadwal();
      }
    });
  }
</script>