
<div class="">
  
  <div class="clearfix"></div>
  
 
  
    <div class="row">
      <div class="col-md-4">
         <form action="<?php echo base_url() ?>prodi/add" method="post">
           <div class="input-group">
            <input class="form-control" name="nama" placeholder="Nama Prodi" type="text">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-primary">Tambah</button>
            </span>
          </div> 
          </form>
      
     <!-- <div class="col-md-6 col-sm-12 col-xs-12"> -->
        <div class="x_panel">
          <!-- <div class="row x_title">
          <div class="col-md-12">
            <h3>Daftar Prodi</h3>
          </div>
            
            <div class="clearfix"></div>
          </div> -->
          <div class="x_content">
            <table class="table table-responsive">
              <thead>
                <th>#</th>
                <th>Nama Prodi</th>
                <th width="50" align="center">Option</th>
              </thead>
              <tbody>
                <?php $no=1 ?>
                <?php foreach ($prodi as $key): ?>
                  <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $key->nama_prodi ?></td>
                    <td align="center">
                    <div class="button-group">
                      <a href="<?php echo base_url()."prodi/delete/".$key->id_prodi.""?>"><i class="fa fa-trash"></i></a>
                    </div>
                  </td>
                  </tr>
                  <?php $no++ ?>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      <!-- </div> -->
    </div>
  </div>
</div>
