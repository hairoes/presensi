<?php

class Sidebar_menu extends Widget {

    public function display($data) {
        
        if (!isset($data['akamemik'])) {
            $data['akademik'] = array('KRS' => 'krs', 'Mata Kuliah' => 'makul', 'Prodi' => 'prodi');
        }
        $this->load->model('m_akademik');
        $data['prodi'] = $this->m_akademik->get_prodi();
        $this->view('widgets/sidebar_menu', $data);
    }
    
}